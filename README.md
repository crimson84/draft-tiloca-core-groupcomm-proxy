# Proxy Operations for CoAP Group Communication

This is the working area for the individual Internet-Draft, "Proxy Operations for CoAP Group Communication".

* [Editor's copy](https://crimson84.gitlab.io/draft-tiloca-core-groupcomm-proxy)
* [Individual Draft](https://datatracker.ietf.org/doc/html/draft-tiloca-core-groupcomm-proxy)
* [Compare Individual Draft and Editor's copy](https://tools.ietf.org/rfcdiff?url1=https://tools.ietf.org/id/draft-tiloca-core-groupcomm-proxy.txt&url2=https://crimson84.gitlab.io/draft-tiloca-core-groupcomm-proxy)


## Building the Draft

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

This requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/main/doc/SETUP.md).


## Contributing

See the
[guidelines for contributions](https://github.com/https:/gitlab.com/blob/master/CONTRIBUTING.md).
